#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):109051904:885a6c0d8258e9d2ce7977ef69646e4df5b82bab; then
  applypatch \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot$(getprop ro.boot.slot_suffix):67108864:9f761d6721f12fb2a317af6ce12a6fb829d87c59 \
          --target EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):109051904:885a6c0d8258e9d2ce7977ef69646e4df5b82bab && \
      (log -t install_recovery "Installing new recovery image: succeeded" && setprop vendor.ota.recovery.status 200) || \
      (log -t install_recovery "Installing new recovery image: failed" && setprop vendor.ota.recovery.status 454)
else
  log -t install_recovery "Recovery image already installed" && setprop vendor.ota.recovery.status 200
fi

